//
//  FRPSearchResult.m
//  Weather?
//
//  Created by Francisco Rodríguez on 29/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPSearchResult.h"

@implementation FRPSearchResult

- (id)initWithJSONDict:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        NSArray *areaName = [json valueForKey:@"areaName"];
        _name = [areaName[0] valueForKey:@"value"];
        
        NSArray *country = [json valueForKey:@"country"];
        _country = [country[0] valueForKey:@"value"];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        _name = [aDecoder decodeObjectForKey:@"areaName"];
        _country = [aDecoder decodeObjectForKey:@"country"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"areaName"];
    [aCoder encodeObject:self.country forKey:@"country"];
}

@end
