//
//  FRPForecastFetcher.m
//  Weather?
//
//  Created by Francisco Rodríguez on 22/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPForecastFetcher.h"
#import "FRPNetworkActivityIndicatorManager.h"
#import "FRPCity.h"
#import "FRPSearchResult.h"

#define FRP_WEATHER_BASE_URL @"http://api.worldweatheronline.com"

@implementation FRPForecastFetcher

- (void)updateCityForecast:(FRPCity *)city withCompletionBlock:(void (^)(NSError *))completion
{
    NSString *urlString = [[NSString stringWithFormat:@"%@/free/v1/weather.ashx?q=%@&format=json&num_of_days=5&key=x5rwm8v5eceu5tuhdgx29sdw", FRP_WEATHER_BASE_URL, city.name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:20.0];
    
    [FRPNetworkActivityIndicatorManager startLoad];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [FRPNetworkActivityIndicatorManager stopLoad];
                               if (!connectionError) {
                                   NSError *error;
                                   NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (jsonDict && !error) {
                                       [city fillWithJSONDict:jsonDict];
                                   } else {
                                       NSLog(@"updateCityForecast: error parsing JSON: %@", error);
                                   }
                                   
                                   if (completion) {
                                       completion(error);
                                   }
                                } else {
                                   NSLog(@"updateCityForecast: error: %@", connectionError);
                               }
                           }];

}

- (void)searchCitiesWithName:(NSString *)name withCompletionBlock:(void (^)(NSArray *))completion
{
    NSString *urlString = [[NSString stringWithFormat:@"%@/free/v1/search.ashx?q=%@&format=json&key=x5rwm8v5eceu5tuhdgx29sdw", FRP_WEATHER_BASE_URL, name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:20.0];
    
    [FRPNetworkActivityIndicatorManager startLoad];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [FRPNetworkActivityIndicatorManager stopLoad];
                               if (!connectionError) {
                                   NSError *error;
                                   NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (jsonDict && !error) {
                                       NSArray *results = [jsonDict valueForKeyPath:@"search_api.result"];
                                       NSMutableArray *objects = [[NSMutableArray alloc] initWithCapacity:results.count];
                                       for (NSDictionary *result in results) {
                                           FRPSearchResult *city = [[FRPSearchResult alloc] initWithJSONDict:result];
                                           [objects addObject:city];
                                       }
                                       if (completion) {
                                           completion(objects);
                                       }
                                   } else {
                                       NSLog(@"updateCityForecast: error parsing JSON: %@", error);
                                       if (completion) {
                                           completion(nil);
                                       }
                                   }
                               } else {
                                   NSLog(@"updateCityForecast: error: %@", connectionError);
                               }
                           }];

}

@end
