#import "_FRPCity.h"

@interface FRPCity : _FRPCity

+ (instancetype)createCityWithName:(NSString *)name inContext:(NSManagedObjectContext *)contextOrNil;

- (void)fillWithJSONDict:(NSDictionary *)jsonDict;

@end
