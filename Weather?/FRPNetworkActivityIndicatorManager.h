//
//  FRPNetworkActivityIndicatorManager.h
//  Weather?
//
//  Created by Francisco Rodríguez on 22/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRPNetworkActivityIndicatorManager : NSObject

+ (void)startLoad;
+ (void)stopLoad;

@end
