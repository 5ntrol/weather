#import "FRPForecast.h"


@interface FRPForecast ()

@end


@implementation FRPForecast

- (void)fillWithJSONDict:(NSDictionary *)jsonDict
{
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
    }
    
    NSString *date = [jsonDict valueForKey:@"date"];
    self.date = [formatter dateFromString:date];
    self.precipMM = [jsonDict valueForKey:@"precipMM"];
    self.tempMaxC = [jsonDict valueForKey:@"tempMaxC"];
    self.tempMinC = [jsonDict valueForKey:@"tempMinC"];
    self.weatherCode = [jsonDict valueForKey:@"weatherCode"];
    self.weatherDesc = [[jsonDict valueForKey:@"weatherDesc"][0] valueForKey:@"value"];
    self.weatherIconUrl = [[jsonDict valueForKey:@"weatherIconUrl"][0] valueForKey:@"value"];
    self.windspeedKmph = [jsonDict valueForKey:@"windspeedKmph"];
}

@end
