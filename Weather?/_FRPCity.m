// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FRPCity.m instead.

#import "_FRPCity.h"

const struct FRPCityAttributes FRPCityAttributes = {
	.creationDate = @"creationDate",
	.humidity = @"humidity",
	.imageUrl = @"imageUrl",
	.name = @"name",
	.observation_time = @"observation_time",
	.temp_C = @"temp_C",
	.weatherCode = @"weatherCode",
	.weatherDesc = @"weatherDesc",
	.weatherIconUrl = @"weatherIconUrl",
	.windspeedKmph = @"windspeedKmph",
};

const struct FRPCityRelationships FRPCityRelationships = {
	.forecast = @"forecast",
};

const struct FRPCityFetchedProperties FRPCityFetchedProperties = {
};

@implementation FRPCityID
@end

@implementation _FRPCity

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"City" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"City";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"City" inManagedObjectContext:moc_];
}

- (FRPCityID*)objectID {
	return (FRPCityID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic creationDate;






@dynamic humidity;






@dynamic imageUrl;






@dynamic name;






@dynamic observation_time;






@dynamic temp_C;






@dynamic weatherCode;






@dynamic weatherDesc;






@dynamic weatherIconUrl;






@dynamic windspeedKmph;






@dynamic forecast;

	
- (NSMutableSet*)forecastSet {
	[self willAccessValueForKey:@"forecast"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"forecast"];
  
	[self didAccessValueForKey:@"forecast"];
	return result;
}
	






@end
