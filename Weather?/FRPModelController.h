//
//  FRPModelController.h
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FRPCityForecastViewController;

@interface FRPModelController : NSObject <UIPageViewControllerDataSource, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, readonly) NSUInteger numPages;

+ (instancetype)sharedInstance;

- (void)createCityWithName:(NSString *)name;

- (FRPCityForecastViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(FRPCityForecastViewController *)viewController;

@end
