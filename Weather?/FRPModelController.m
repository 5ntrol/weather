//
//  FRPModelController.m
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPModelController.h"
#import "FRPCityForecastViewController.h"
#import "FRPCoreDataManager.h"
#import "FRPCity.h"

@interface FRPModelController()

@property (strong, nonatomic) NSFetchedResultsController *citiesResultsController;

@end

@implementation FRPModelController

static FRPModelController *instance;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    
    return instance;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self.citiesResultsController performFetch:nil];
    }
    return self;
}

- (NSUInteger)numPages
{
    return self.citiesResultsController.fetchedObjects.count;
}

- (void)createCityWithName:(NSString *)name
{
    [FRPCity createCityWithName:name inContext:nil];
    [self.citiesResultsController performFetch:nil];
}

- (FRPCityForecastViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard
{   
    if ((self.citiesResultsController.fetchedObjects.count == 0) || (index >= self.citiesResultsController.fetchedObjects.count)) {
        return nil;
    }
    
    FRPCityForecastViewController *dataViewController = [storyboard instantiateViewControllerWithIdentifier:@"FRPDataViewController"];
    dataViewController.city = [self.citiesResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    return dataViewController;
}

- (NSUInteger)indexOfViewController:(FRPCityForecastViewController *)viewController
{
    return [self.citiesResultsController indexPathForObject:viewController.city].row;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(FRPCityForecastViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(FRPCityForecastViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [[self.citiesResultsController fetchedObjects] count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.citiesResultsController.sections[section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    FRPCity *city = [self.citiesResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = city.name;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        FRPCity *city = [self.citiesResultsController objectAtIndexPath:indexPath];
        [city.managedObjectContext deleteObject:city];
        [self.citiesResultsController performFetch:nil];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Private methods

- (NSFetchedResultsController *)citiesResultsController
{
    if (!_citiesResultsController) {
        NSManagedObjectContext *context = [FRPCoreDataManager sharedInstance].mainObjectContext;
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[FRPCity entityName]];
        
        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"creationDate" ascending:NO];
        fetchRequest.sortDescriptors = @[sort];
        
        NSFetchedResultsController *theFetchedResultsController =
        [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                            managedObjectContext:context
                                              sectionNameKeyPath:nil
                                                       cacheName:nil];
        _citiesResultsController = theFetchedResultsController;
    }
    
    return _citiesResultsController;
}

@end
