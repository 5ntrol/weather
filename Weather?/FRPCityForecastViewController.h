//
//  FRPDataViewController.h
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FRPCity;
@interface FRPCityForecastViewController : UIViewController

@property (nonatomic, strong) FRPCity *city;

- (void)downloadWeatherData;

@end
