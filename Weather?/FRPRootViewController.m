//
//  FRPRootViewController.m
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPRootViewController.h"
#import "FRPModelController.h"
#import "FRPCityForecastViewController.h"
#import "FRPCitiesViewController.h"

@interface FRPRootViewController () <UIPageViewControllerDelegate>

@property (strong, nonatomic) NSNumber *visiblePage;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (IBAction)refreshPressed:(id)sender;

@end

@implementation FRPRootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];

    self.pageViewController.dataSource = [FRPModelController sharedInstance];
    self.pageViewController.delegate = self;

    [self addChildViewController:self.pageViewController];
    [self.view insertSubview:self.pageViewController.view belowSubview:self.pageControl];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    FRPCityForecastViewController *startingViewController = [[FRPModelController sharedInstance] viewControllerAtIndex:self.visiblePage.integerValue storyboard:self.storyboard];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageControl.numberOfPages = [FRPModelController sharedInstance].numPages;
    self.pageControl.currentPage = self.visiblePage.integerValue;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)unwindToThisViewController:(UIStoryboardSegue *)unwindSegue
{
}

#pragma mark - State preservation

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSNumber *visibleCity = @([[FRPModelController sharedInstance] indexOfViewController:self.pageViewController.viewControllers[0]]);
    [coder encodeObject:visibleCity forKey:@"visibleCity"];
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    self.visiblePage = [coder decodeObjectForKey:@"visibleCity"];
    [super decodeRestorableStateWithCoder:coder];
}

#pragma mark - UIPageViewControllerDelegate methods

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed && pageViewController.viewControllers.count) {
        self.pageControl.currentPage = [[FRPModelController sharedInstance] indexOfViewController:pageViewController.viewControllers[0]];
    }
}

#pragma mark - Private methods

- (IBAction)refreshPressed:(id)sender
{
    if (self.pageViewController.viewControllers.count) {
        FRPCityForecastViewController *vc = self.pageViewController.viewControllers[0];
        [vc downloadWeatherData];
    }
}

@end
