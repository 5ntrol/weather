// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FRPForecast.m instead.

#import "_FRPForecast.h"

const struct FRPForecastAttributes FRPForecastAttributes = {
	.date = @"date",
	.precipMM = @"precipMM",
	.tempMaxC = @"tempMaxC",
	.tempMinC = @"tempMinC",
	.weatherCode = @"weatherCode",
	.weatherDesc = @"weatherDesc",
	.weatherIconUrl = @"weatherIconUrl",
	.windspeedKmph = @"windspeedKmph",
};

const struct FRPForecastRelationships FRPForecastRelationships = {
	.city = @"city",
};

const struct FRPForecastFetchedProperties FRPForecastFetchedProperties = {
};

@implementation FRPForecastID
@end

@implementation _FRPForecast

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Forecast" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Forecast";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Forecast" inManagedObjectContext:moc_];
}

- (FRPForecastID*)objectID {
	return (FRPForecastID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic date;






@dynamic precipMM;






@dynamic tempMaxC;






@dynamic tempMinC;






@dynamic weatherCode;






@dynamic weatherDesc;






@dynamic weatherIconUrl;






@dynamic windspeedKmph;






@dynamic city;

	






@end
