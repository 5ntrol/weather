//
//  FRPAppDelegate.m
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPAppDelegate.h"
#import "FRPCity.h"

@interface FRPAppDelegate ()

- (void)addDefaultCitiesIfNeeded;

@end

@implementation FRPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self addDefaultCitiesIfNeeded];
    return YES;
}

#pragma mark - State restoration

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

#pragma mark - Private methods

- (void)addDefaultCitiesIfNeeded
{
    static NSString *key = @"FRPWeatherDefaultsInstalled";
    
    BOOL added = [[NSUserDefaults standardUserDefaults] boolForKey:key];
    if (!added) {
        [FRPCity createCityWithName:@"Dublin" inContext:nil];
        [FRPCity createCityWithName:@"London" inContext:nil];
        [FRPCity createCityWithName:@"New York" inContext:nil];
        [FRPCity createCityWithName:@"Barcelona" inContext:nil];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:key];
    }
}

@end
