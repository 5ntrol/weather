#import "_FRPForecast.h"

@interface FRPForecast : _FRPForecast

- (void)fillWithJSONDict:(NSDictionary *)jsonDict;

@end
