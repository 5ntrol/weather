//
//  FRPSearchResult.h
//  Weather?
//
//  Created by Francisco Rodríguez on 29/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRPSearchResult : NSObject <NSCoding>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *country;

- (id)initWithJSONDict:(NSDictionary *)json;

@end
