//
//  FRPAppDelegate.h
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
