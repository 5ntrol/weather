// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FRPForecast.h instead.

#import <CoreData/CoreData.h>


extern const struct FRPForecastAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *precipMM;
	__unsafe_unretained NSString *tempMaxC;
	__unsafe_unretained NSString *tempMinC;
	__unsafe_unretained NSString *weatherCode;
	__unsafe_unretained NSString *weatherDesc;
	__unsafe_unretained NSString *weatherIconUrl;
	__unsafe_unretained NSString *windspeedKmph;
} FRPForecastAttributes;

extern const struct FRPForecastRelationships {
	__unsafe_unretained NSString *city;
} FRPForecastRelationships;

extern const struct FRPForecastFetchedProperties {
} FRPForecastFetchedProperties;

@class FRPCity;










@interface FRPForecastID : NSManagedObjectID {}
@end

@interface _FRPForecast : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FRPForecastID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* precipMM;



//- (BOOL)validatePrecipMM:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* tempMaxC;



//- (BOOL)validateTempMaxC:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* tempMinC;



//- (BOOL)validateTempMinC:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* weatherCode;



//- (BOOL)validateWeatherCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* weatherDesc;



//- (BOOL)validateWeatherDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* weatherIconUrl;



//- (BOOL)validateWeatherIconUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* windspeedKmph;



//- (BOOL)validateWindspeedKmph:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) FRPCity *city;

//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;





@end

@interface _FRPForecast (CoreDataGeneratedAccessors)

@end

@interface _FRPForecast (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSString*)primitivePrecipMM;
- (void)setPrimitivePrecipMM:(NSString*)value;




- (NSString*)primitiveTempMaxC;
- (void)setPrimitiveTempMaxC:(NSString*)value;




- (NSString*)primitiveTempMinC;
- (void)setPrimitiveTempMinC:(NSString*)value;




- (NSString*)primitiveWeatherCode;
- (void)setPrimitiveWeatherCode:(NSString*)value;




- (NSString*)primitiveWeatherDesc;
- (void)setPrimitiveWeatherDesc:(NSString*)value;




- (NSString*)primitiveWeatherIconUrl;
- (void)setPrimitiveWeatherIconUrl:(NSString*)value;




- (NSString*)primitiveWindspeedKmph;
- (void)setPrimitiveWindspeedKmph:(NSString*)value;





- (FRPCity*)primitiveCity;
- (void)setPrimitiveCity:(FRPCity*)value;


@end
