//
//  FRPCityAddViewController.m
//  Weather?
//
//  Created by Francisco Rodríguez on 29/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPCityAddViewController.h"
#import "FRPForecastFetcher.h"
#import "FRPSearchResult.h"
#import "FRPModelController.h"

@interface FRPCityAddViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UITextField *cityNameField;

@property (nonatomic, strong) FRPForecastFetcher *fetcher;
@property (nonatomic, strong) NSArray *searchResults;

- (IBAction)searchTextChanged:(UITextField *)sender;

@end

@implementation FRPCityAddViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    FRPSearchResult *result = self.searchResults[indexPath.row];
    
    cell.textLabel.text = result.name;
    cell.detailTextLabel.text = result.country;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FRPSearchResult *selectedResult = self.searchResults[indexPath.row];
    [[FRPModelController sharedInstance] createCityWithName:selectedResult.name];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - State preservation

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.cityNameField.text forKey:@"citySearchText"];
    [coder encodeObject:self.searchResults forKey:@"searchResults"];
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    self.cityNameField.text = [coder decodeObjectForKey:@"citySearchText"];
    self.searchResults = [coder decodeObjectForKey:@"searchResults"];
    
    [super decodeRestorableStateWithCoder:coder];
}

#pragma mark - Private methods

- (FRPForecastFetcher *)fetcher
{
    if (!_fetcher) {
        _fetcher = [FRPForecastFetcher new];
    }
    
    return _fetcher;
}

- (IBAction)searchTextChanged:(UITextField *)sender
{
    if (sender.text.length > 2) {
        __weak FRPCityAddViewController *selfRef = self;
        [self.fetcher searchCitiesWithName:sender.text withCompletionBlock:^(NSArray *results) {
            selfRef.searchResults = results;
            [selfRef.tableView reloadData];
        }];
    } else {
        self.searchResults = nil;
        [self.tableView reloadData];
    }
}

@end
