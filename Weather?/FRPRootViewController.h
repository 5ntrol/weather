//
//  FRPRootViewController.h
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRPRootViewController : UIViewController

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
