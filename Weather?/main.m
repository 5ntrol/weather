//
//  main.m
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FRPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FRPAppDelegate class]));
    }
}
