#import "FRPCity.h"
#import "FRPForecast.h"
#import "FRPCoreDataManager.h"

@interface FRPCity ()

@end

@implementation FRPCity

+ (instancetype)createCityWithName:(NSString *)name inContext:(NSManagedObjectContext *)contextOrNil
{
    NSManagedObjectContext *context = CONTEXT_FROM_CONTEXTORNIL(contextOrNil);
    
    __block FRPCity *city;
    [context performBlockAndWait:^{
        city = [FRPCity insertInManagedObjectContext:context];
        city.name = name;
        city.creationDate = [NSDate date];
    }];

    return city;
}

- (void)fillWithJSONDict:(NSDictionary *)jsonDict
{
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm a"];
    }
    
    NSManagedObjectContext *context = self.managedObjectContext;
        
    [context performBlockAndWait:^{
        NSDictionary *currentCondition = [jsonDict valueForKeyPath:@"data.current_condition"][0];
        self.humidity = [currentCondition valueForKey:@"humidity"];
        NSString *date = [currentCondition valueForKey:@"observation_time"];
        self.observation_time = [formatter dateFromString:date];
        self.temp_C = [currentCondition valueForKey:@"temp_C"];
        self.weatherCode = [currentCondition valueForKey:@"weatherCode"];
        self.weatherDesc = [[currentCondition valueForKey:@"weatherDesc"][0] valueForKey:@"value"];
        self.weatherIconUrl = [[currentCondition valueForKey:@"weatherIconUrl"][0] valueForKey:@"value"];
        
        [self removeForecast:self.forecast];
        
        NSArray *forecasts = [jsonDict valueForKeyPath:@"data.weather"];
        for (NSDictionary *forecastDict in forecasts) {
            FRPForecast *forecast = [FRPForecast insertInManagedObjectContext:context];
            [forecast fillWithJSONDict:forecastDict];
            
            [self addForecastObject:forecast];
        }
    }];
}

@end
