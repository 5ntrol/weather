//
//  FRPForecastFetcher.h
//  Weather?
//
//  Created by Francisco Rodríguez on 22/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FRPCity;
@interface FRPForecastFetcher : NSObject

- (void)updateCityForecast:(FRPCity *)city withCompletionBlock:(void (^)(NSError *err))completion;
- (void)searchCitiesWithName:(NSString *)name withCompletionBlock:(void (^)(NSArray *results))completion;

@end
