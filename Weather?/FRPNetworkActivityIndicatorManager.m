//
//  FRPNetworkActivityIndicatorManager.m
//  Weather?
//
//  Created by Francisco Rodríguez on 22/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPNetworkActivityIndicatorManager.h"

@interface FRPNetworkActivityIndicatorManager ()

+ (void)updateLoadCountWithDelta:(NSInteger)countDelta;

@end

static NSInteger _loadsCount = 0;

@implementation FRPNetworkActivityIndicatorManager

+ (void)startLoad {
    [self updateLoadCountWithDelta:1];
}

+ (void)stopLoad {
    [self updateLoadCountWithDelta:-1];
}

+ (void)updateLoadCountWithDelta:(NSInteger)countDelta
{
    @synchronized([UIApplication sharedApplication]) {
        _loadsCount += countDelta;
        _loadsCount = (_loadsCount < 0) ? 0 : _loadsCount ;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = _loadsCount > 0;
    }
}

@end
