//
//  FRPDataViewController.m
//  Weather?
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPCityForecastViewController.h"
#import "FRPForecastFetcher.h"
#import "FRPCity.h"
#import "FRPForecast.h"
#import "UIImageView+WebCache.h"
#import "FRPFlickrFetcher.h"
#import "FRPCitiesViewController.h"

@interface FRPCityForecastViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *currentTempLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentConditionLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayTempsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *currentConditionImageView;

@property (weak, nonatomic) IBOutlet UILabel *forecastDay1;
@property (weak, nonatomic) IBOutlet UILabel *forecastDay2;
@property (weak, nonatomic) IBOutlet UILabel *forecastDay3;
@property (weak, nonatomic) IBOutlet UILabel *forecastDay4;

@property (weak, nonatomic) IBOutlet UIImageView *forecastIcon1;
@property (weak, nonatomic) IBOutlet UIImageView *forecastIcon2;
@property (weak, nonatomic) IBOutlet UIImageView *forecastIcon3;
@property (weak, nonatomic) IBOutlet UIImageView *forecastIcon4;

@property (weak, nonatomic) IBOutlet UILabel *forecastTemps1;
@property (weak, nonatomic) IBOutlet UILabel *forecastTemps2;
@property (weak, nonatomic) IBOutlet UILabel *forecastTemps3;
@property (weak, nonatomic) IBOutlet UILabel *forecastTemps4;

- (void)updateForecastView;
- (void)downloadBackgroundImage;

@end

@implementation FRPCityForecastViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.city.imageUrl) {
        [self.backgroundImageView setImageWithURL:[NSURL URLWithString:self.city.imageUrl]];
    } else {
        [self downloadBackgroundImage];
    }
    
    [self updateForecastView];
    [self downloadWeatherData];
}

- (void)downloadWeatherData
{
    self.cityNameLabel.text = NSLocalizedString(@"Updating...", nil);
    FRPForecastFetcher *fetcher = [FRPForecastFetcher new];
    __weak FRPCityForecastViewController *selfRef = self;
    [fetcher updateCityForecast:self.city withCompletionBlock:^(NSError *err) {
        selfRef.cityNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ forecast", nil), self.city.name];
        if (!err) {
            [selfRef updateForecastView];
        }
    }];
}

#pragma mark - Private methods

- (void)updateForecastView
{
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE"];
    }
    
    if (self.city.forecast.count) {
        NSMutableArray *forecasts = [[NSMutableArray alloc] initWithArray:self.city.forecast.allObjects];
        [forecasts sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [((FRPForecast *)obj1).date compare:((FRPForecast *)obj2).date];
        }];
        
        self.currentTempLabel.text = [NSString stringWithFormat:@"%@°", self.city.temp_C];
        self.currentConditionLabel.text = self.city.weatherDesc;
        [self.currentConditionImageView setImageWithURL:[NSURL URLWithString:self.city.weatherIconUrl]];
        FRPForecast *todaysForecast = forecasts[0];
        self.todayTempsLabel.text = [NSString stringWithFormat:@"%@° / %@°", todaysForecast.tempMinC, todaysForecast.tempMaxC];
        
        for (int i = 1; i < forecasts.count; i++) {
            FRPForecast *forecast = forecasts[i];
            [self setValue:[formatter stringFromDate:forecast.date] forKeyPath:[NSString stringWithFormat:@"forecastDay%i.text", i]];
            UIImageView *icon = [self valueForKey:[NSString stringWithFormat:@"forecastIcon%i", i]];
            [icon performSelector:@selector(setImageWithURL:) withObject:[NSURL URLWithString:forecast.weatherIconUrl]];
            [self setValue:[NSString stringWithFormat:@"%@° / %@°", forecast.tempMinC, forecast.tempMaxC] forKeyPath:[NSString stringWithFormat:@"forecastTemps%i.text", i]];
        }
    }
}

- (void)downloadBackgroundImage
{
    FRPFlickrFetcher *fetcher = [FRPFlickrFetcher new];
    __weak FRPCityForecastViewController *selfRef = self;
    [fetcher getBackgroundUrlForCity:self.city.name withCompletionBlock:^(NSURL *path, NSError *err) {
        selfRef.city.imageUrl = path.absoluteString;
        [selfRef.backgroundImageView setImageWithURL:path];
    }];
}

@end
