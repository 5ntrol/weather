//
//  FRPCitiesViewController.m
//  Weather?
//
//  Created by Francisco Rodríguez on 29/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPCitiesViewController.h"
#import "FRPModelController.h"

@interface FRPCitiesViewController ()

@end

@implementation FRPCitiesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.dataSource = [FRPModelController sharedInstance];
    self.tableView.delegate = [FRPModelController sharedInstance];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

@end
