//
//  FRPFlickrFetcher.h
//  Weather?
//
//  Created by Francisco Rodríguez on 26/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRPFlickrFetcher : NSObject

- (void)getBackgroundUrlForCity:(NSString *)name withCompletionBlock:(void (^)(NSURL *path, NSError *err))completion;

@end
