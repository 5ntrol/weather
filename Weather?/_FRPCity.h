// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FRPCity.h instead.

#import <CoreData/CoreData.h>


extern const struct FRPCityAttributes {
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *humidity;
	__unsafe_unretained NSString *imageUrl;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *observation_time;
	__unsafe_unretained NSString *temp_C;
	__unsafe_unretained NSString *weatherCode;
	__unsafe_unretained NSString *weatherDesc;
	__unsafe_unretained NSString *weatherIconUrl;
	__unsafe_unretained NSString *windspeedKmph;
} FRPCityAttributes;

extern const struct FRPCityRelationships {
	__unsafe_unretained NSString *forecast;
} FRPCityRelationships;

extern const struct FRPCityFetchedProperties {
} FRPCityFetchedProperties;

@class FRPForecast;












@interface FRPCityID : NSManagedObjectID {}
@end

@interface _FRPCity : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FRPCityID*)objectID;





@property (nonatomic, strong) NSDate* creationDate;



//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* humidity;



//- (BOOL)validateHumidity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* imageUrl;



//- (BOOL)validateImageUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* observation_time;



//- (BOOL)validateObservation_time:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* temp_C;



//- (BOOL)validateTemp_C:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* weatherCode;



//- (BOOL)validateWeatherCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* weatherDesc;



//- (BOOL)validateWeatherDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* weatherIconUrl;



//- (BOOL)validateWeatherIconUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* windspeedKmph;



//- (BOOL)validateWindspeedKmph:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *forecast;

- (NSMutableSet*)forecastSet;





@end

@interface _FRPCity (CoreDataGeneratedAccessors)

- (void)addForecast:(NSSet*)value_;
- (void)removeForecast:(NSSet*)value_;
- (void)addForecastObject:(FRPForecast*)value_;
- (void)removeForecastObject:(FRPForecast*)value_;

@end

@interface _FRPCity (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(NSDate*)value;




- (NSString*)primitiveHumidity;
- (void)setPrimitiveHumidity:(NSString*)value;




- (NSString*)primitiveImageUrl;
- (void)setPrimitiveImageUrl:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSDate*)primitiveObservation_time;
- (void)setPrimitiveObservation_time:(NSDate*)value;




- (NSString*)primitiveTemp_C;
- (void)setPrimitiveTemp_C:(NSString*)value;




- (NSString*)primitiveWeatherCode;
- (void)setPrimitiveWeatherCode:(NSString*)value;




- (NSString*)primitiveWeatherDesc;
- (void)setPrimitiveWeatherDesc:(NSString*)value;




- (NSString*)primitiveWeatherIconUrl;
- (void)setPrimitiveWeatherIconUrl:(NSString*)value;




- (NSString*)primitiveWindspeedKmph;
- (void)setPrimitiveWindspeedKmph:(NSString*)value;





- (NSMutableSet*)primitiveForecast;
- (void)setPrimitiveForecast:(NSMutableSet*)value;


@end
