//
//  FRPFlickrFetcher.m
//  Weather?
//
//  Created by Francisco Rodríguez on 26/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import "FRPFlickrFetcher.h"
#import "FRPNetworkActivityIndicatorManager.h"

#define FRP_FLICKR_API_KEY @"76e8179e3438d6c351999218996a01c7"

@interface FRPFlickrFetcher ()

- (void)getPhotoIdForPlaceId:(NSString *)placeId withCompletionBlock:(void (^)(NSURL *, NSError *))completion;
- (void)getUrlForPhotoId:(NSString *)photoId withCompletionBlock:(void (^)(NSURL *, NSError *))completion;

@end

@implementation FRPFlickrFetcher

- (void)getBackgroundUrlForCity:(NSString *)name withCompletionBlock:(void (^)(NSURL *, NSError *))completion
{
    NSString *urlString = [[NSString stringWithFormat:@"http://api.flickr.com/services/rest/?method=flickr.places.find&api_key=%@&query=%@&format=json&nojsoncallback=1", FRP_FLICKR_API_KEY, name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:20.0];
    
    [FRPNetworkActivityIndicatorManager startLoad];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [FRPNetworkActivityIndicatorManager stopLoad];
                               if (!connectionError) {
                                   NSError *error;
                                   NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (jsonDict && !error) {
                                       NSArray *places = [jsonDict valueForKeyPath:@"places.place"];
                                       if (places.count > 0) {
                                           NSDictionary *firstMatch = places[0];
                                           NSString *placeId = [firstMatch valueForKey:@"place_id"];
                                           [self getPhotoIdForPlaceId:placeId withCompletionBlock:completion];
                                       }
                                   } else {
                                       NSLog(@"getBackgroundUrlForCity: error parsing JSON: %@", error);
                                   }
                               } else {
                                   NSLog(@"getBackgroundUrlForCity: error: %@", connectionError);
                               }
                           }];
}

#pragma mark - Private methods

- (void)getPhotoIdForPlaceId:(NSString *)placeId withCompletionBlock:(void (^)(NSURL *, NSError *))completion
{
    NSURL *placeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&place_id=%@&per_page=1&tags=landmark&format=json&nojsoncallback=1", FRP_FLICKR_API_KEY, placeId]];
    
    NSMutableURLRequest *placeRequest = [NSMutableURLRequest requestWithURL:placeUrl
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:20.0];
    
    [FRPNetworkActivityIndicatorManager startLoad];
    [NSURLConnection sendAsynchronousRequest:placeRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [FRPNetworkActivityIndicatorManager stopLoad];
                               if (!connectionError) {
                                   NSError *error;
                                   NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (jsonDict && !error) {
                                       NSArray *photos = [jsonDict valueForKeyPath:@"photos.photo"];
                                       if (photos.count > 0) {
                                           NSDictionary *firstMatch = photos[0];
                                           NSString *photoId = [firstMatch valueForKey:@"id"];
                                           [self getUrlForPhotoId:photoId withCompletionBlock:completion];
                                       }
                                   } else {
                                       NSLog(@"getPhotoIdForPlaceId: error parsing JSON: %@", error);
                                   }
                               } else {
                                   NSLog(@"getPhotoIdForPlaceId: error: %@", connectionError);
                               }
                           }];
}

- (void)getUrlForPhotoId:(NSString *)photoId withCompletionBlock:(void (^)(NSURL *, NSError *))completion
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=%@&photo_id=%@&format=json&nojsoncallback=1", FRP_FLICKR_API_KEY, photoId]];
    
    NSMutableURLRequest *sizesRequest = [NSMutableURLRequest requestWithURL:url
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:20.0];
    
    [FRPNetworkActivityIndicatorManager startLoad];
    [NSURLConnection sendAsynchronousRequest:sizesRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [FRPNetworkActivityIndicatorManager stopLoad];
                               if (!connectionError) {
                                   NSError *error;
                                   NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (jsonDict && !error) {
                                       NSArray *sizes = [jsonDict valueForKeyPath:@"sizes.size"];
                                       for (NSDictionary *size in sizes) {
                                           NSString *label = [size valueForKey:@"label"];
                                           if ([label rangeOfString:@"edium"].location != NSNotFound) {
                                               NSURL *photoUrl = [NSURL URLWithString:[size valueForKey:@"source"]];
                                               if (completion) {
                                                   completion(photoUrl, nil);
                                               }
                                               break;
                                           }
                                       }
                                   } else {
                                       NSLog(@"getUrlForPhotoId: error parsing JSON: %@", error);
                                   }
                               } else {
                                   NSLog(@"getUrlForPhotoId: error: %@", connectionError);
                               }
                           }];
}

@end
