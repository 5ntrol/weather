//
//  Weather_Tests.m
//  Weather?Tests
//
//  Created by Francisco Rodríguez on 21/01/14.
//  Copyright (c) 2014 Francisco Rodriguez. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FRPCity.h"
#import "FRPFlickrFetcher.h"
#import "FRPForecastFetcher.h"
#import "FRPSearchResult.h"

@interface Weather_Tests : XCTestCase

@end

@implementation Weather_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testForecastParsing
{
    NSString *JSON = @"{\"data\":{\"current_condition\":[{\"cloudcover\":\"100\",\"humidity\":\"81\",\"observation_time\":\"09:55 PM\",\"precipMM\":\"0.0\",\"pressure\":\"1011\",\"temp_C\":\"9\",\"temp_F\":\"48\",\"visibility\":\"10\",\"weatherCode\":\"122\",\"weatherDesc\":[{\"value\":\"Overcast\"}],\"weatherIconUrl\":[{\"value\":\"http:\\/\\/cdn.worldweatheronline.net\\/images\\/wsymbols01_png_64\\/wsymbol_0004_black_low_cloud.png\"}],\"winddir16Point\":\"WSW\",\"winddirDegree\":\"250\",\"windspeedKmph\":\"11\",\"windspeedMiles\":\"7\"}],\"request\":[{\"query\":\"London, United Kingdom\",\"type\":\"City\"}],\"weather\":[{\"date\":\"2014-01-22\",\"precipMM\":\"4.4\",\"tempMaxC\":\"10\",\"tempMaxF\":\"50\",\"tempMinC\":\"1\",\"tempMinF\":\"35\",\"weatherCode\":\"113\",\"weatherDesc\":[{\"value\":\"Sunny\"}],\"weatherIconUrl\":[{\"value\":\"http:\\/\\/cdn.worldweatheronline.net\\/images\\/wsymbols01_png_64\\/wsymbol_0001_sunny.png\"}],\"winddir16Point\":\"WSW\",\"winddirDegree\":\"254\",\"winddirection\":\"WSW\",\"windspeedKmph\":\"24\",\"windspeedMiles\":\"15\"},{\"date\":\"2014-01-23\",\"precipMM\":\"2.4\",\"tempMaxC\":\"6\",\"tempMaxF\":\"43\",\"tempMinC\":\"2\",\"tempMinF\":\"35\",\"weatherCode\":\"293\",\"weatherDesc\":[{\"value\":\"Patchy light rain\"}],\"weatherIconUrl\":[{\"value\":\"http:\\/\\/cdn.worldweatheronline.net\\/images\\/wsymbols01_png_64\\/wsymbol_0017_cloudy_with_light_rain.png\"}],\"winddir16Point\":\"WNW\",\"winddirDegree\":\"288\",\"winddirection\":\"WNW\",\"windspeedKmph\":\"23\",\"windspeedMiles\":\"14\"},{\"date\":\"2014-01-24\",\"precipMM\":\"11.9\",\"tempMaxC\":\"6\",\"tempMaxF\":\"43\",\"tempMinC\":\"5\",\"tempMinF\":\"41\",\"weatherCode\":\"296\",\"weatherDesc\":[{\"value\":\"Light rain\"}],\"weatherIconUrl\":[{\"value\":\"http:\\/\\/cdn.worldweatheronline.net\\/images\\/wsymbols01_png_64\\/wsymbol_0017_cloudy_with_light_rain.png\"}],\"winddir16Point\":\"SSE\",\"winddirDegree\":\"152\",\"winddirection\":\"SSE\",\"windspeedKmph\":\"24\",\"windspeedMiles\":\"15\"},{\"date\":\"2014-01-25\",\"precipMM\":\"2.9\",\"tempMaxC\":\"9\",\"tempMaxF\":\"49\",\"tempMinC\":\"1\",\"tempMinF\":\"34\",\"weatherCode\":\"119\",\"weatherDesc\":[{\"value\":\"Cloudy\"}],\"weatherIconUrl\":[{\"value\":\"http:\\/\\/cdn.worldweatheronline.net\\/images\\/wsymbols01_png_64\\/wsymbol_0003_white_cloud.png\"}],\"winddir16Point\":\"W\",\"winddirDegree\":\"265\",\"winddirection\":\"W\",\"windspeedKmph\":\"29\",\"windspeedMiles\":\"18\"},{\"date\":\"2014-01-26\",\"precipMM\":\"7.1\",\"tempMaxC\":\"8\",\"tempMaxF\":\"47\",\"tempMinC\":\"0\",\"tempMinF\":\"32\",\"weatherCode\":\"302\",\"weatherDesc\":[{\"value\":\"Moderate rain\"}],\"weatherIconUrl\":[{\"value\":\"http:\\/\\/cdn.worldweatheronline.net\\/images\\/wsymbols01_png_64\\/wsymbol_0018_cloudy_with_heavy_rain.png\"}],\"winddir16Point\":\"S\",\"winddirDegree\":\"175\",\"winddirection\":\"S\",\"windspeedKmph\":\"34\",\"windspeedMiles\":\"21\"}]}}";
    
    NSError *error;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[JSON dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    XCTAssertNotNil(jsonDict);
    
    FRPCity *cityForecast = [FRPCity createCityWithName:@"testCity" inContext:nil];
    [cityForecast fillWithJSONDict:jsonDict];
    
    XCTAssertEqualObjects(cityForecast.temp_C, @"9");
    XCTAssertEqualObjects(cityForecast.humidity, @"81");
    XCTAssertNotNil(cityForecast.observation_time);
    XCTAssertEqual(cityForecast.forecast.count, 5u);
}

- (void)testFlickrPhotoFetcher
{
    FRPFlickrFetcher *fetcher = [FRPFlickrFetcher new];
    [fetcher getBackgroundUrlForCity:@"salamanca" withCompletionBlock:^(NSURL *path, NSError *err) {
        NSLog(@"URL for salamanca bg: %@", path.absoluteString);

    }];
    
    [[NSRunLoop currentRunLoop]
     runUntilDate:[NSDate dateWithTimeIntervalSinceNow:10]];
}

- (void)testCitiesSearch
{
    FRPForecastFetcher *fetcher = [FRPForecastFetcher new];
    [fetcher searchCitiesWithName:@"Salamanca" withCompletionBlock:^(NSArray *results) {
        for (FRPSearchResult *res in results) {
            NSLog(@"Name: %@ (%@)", res.name, res.country);
        }
    }];
    
    [[NSRunLoop currentRunLoop]
     runUntilDate:[NSDate dateWithTimeIntervalSinceNow:10]];
}

@end
